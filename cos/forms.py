from django import forms


class Cosine_form(forms.Form):
	sentence1 = forms.CharField(max_length =255)
	sentence2 = forms.CharField(max_length =255)
	
class Porter_form(forms.Form):
	word = forms.CharField(max_length =255)
	
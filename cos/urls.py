from django.conf.urls import patterns, include, url
from cos import views

urlpatterns = patterns('',

    url(r'^cosine/', views.index, name='cos'),
    url(r'^porter/', views.porter, name='port'),
    url(r'^', views.main, name='main'),
)

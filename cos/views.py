from django.shortcuts import render,render_to_response
from django.views.decorators.csrf import csrf_exempt
from cos.forms import Cosine_form,Porter_form
from django.template import RequestContext  
from cos.algo import text_to_vector,get_cosine 
from cos.porter_algo import PorterStemmer
# Create your views here.

@csrf_exempt
def main(request):
    return render_to_response('main.html',context_instance=RequestContext(request))


@csrf_exempt
def index(request):
    message = ""
    if request.method == 'GET':
        form =Cosine_form()
        return render_to_response('Cosine/index.html',{'form':form,'message':message},context_instance=RequestContext(request))

    elif request.method == 'POST':
        form = Cosine_form(request.POST)
        if form.is_valid():
        	print request.POST
        	text1 = request.POST['sentence1']
        	text2 = request.POST['sentence2']
        	vector1 = text_to_vector(text1)
        	vector2 = text_to_vector(text2)
        	cosine = get_cosine(vector1,vector2)
        	return render_to_response('Cosine/display.html',{'cosine':cosine},context_instance=RequestContext(request))
            #sentence = request.POST['sentence']
            #data = {'USERNAME':username,'PASSWORD':password}
            #build_apk(data)

        else:
            message = 'Form has errors'
            return render_to_response('Cosine/index.html',{'form':form,'message':message}, context_instance = RequestContext(request))

@csrf_exempt
def porter(request):
    message = ""
    if request.method == 'GET':
        form =Porter_form()
        return render_to_response('Porter/index.html',{'form':form,'message':message},context_instance=RequestContext(request))

    elif request.method == 'POST':
        form = Porter_form(request.POST)
        if form.is_valid():
        	print request.POST
        	word = request.POST['word']
        	p = PorterStemmer()
        	output=""
        	output = p.stem(word,0,len(word)-1)
        	return render_to_response('Porter/display.html',{'stem':output},context_instance=RequestContext(request))
            #sentence = request.POST['sentence']
            #data = {'USERNAME':username,'PASSWORD':password}
            #build_apk(data)

        else:
            message = 'Form has errors'
            return render_to_response('Porter/index.html',{'form':form,'message':message}, context_instance = RequestContext(request))
